#include "CHand.h"

void CHand::vClear()
{
	for (int i = 0; i < m_vpclHand.size(); i++)
		delete(m_vpclHand[i]);
	
	m_vpclHand.clear();
	m_blIsAce = false;
	m_nHandValue = 0;
}

int CHand::nGetValue()
{
	for (auto i = 0; i < m_vpclHand.size(); i++)
	{
		int nTemp = m_vpclHand[i]->nGetValue();

		if (nTemp == 1)
			m_blIsAce = true;

		m_nHandValue += nTemp;
	}
	
	if ((m_blIsAce) && (m_nHandValue <= 11))
		m_nHandValue += 10;

	return m_nHandValue;
}