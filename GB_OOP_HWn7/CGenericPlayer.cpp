#include "CGenericPlayer.h"

CGenericPlayer::CGenericPlayer()
	: m_strName("John Doe"),
	CHand()
{
}

CGenericPlayer::CGenericPlayer(std::string strName)
	: m_strName(strName),
	CHand()
{
}

CGenericPlayer::~CGenericPlayer()
{
	vClear();
}

std::ostream& operator<< (std::ostream& osOut, CGenericPlayer& clCurrentPlayer)
{
	osOut << clCurrentPlayer.m_strName;

	bool blIsShowed = true;

	for (auto it = 0; it < clCurrentPlayer.m_vpclHand.size(); it++)
	{
		if (!(clCurrentPlayer.m_vpclHand[it]->blIsShowed()))
			blIsShowed = false;

		osOut << clCurrentPlayer.m_vpclHand[it] << std::endl;
	}

	if (blIsShowed)
		osOut << clCurrentPlayer.nGetValue();

	return osOut;
}