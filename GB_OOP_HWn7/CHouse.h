#pragma once
#include "CGenericPlayer.h"

class CHouse :
    public CGenericPlayer
{
public:
    CHouse()
        : CGenericPlayer("House")
    {
    }
    ~CHouse() {};

    virtual bool blIsHitting() { return (nGetValue() <= 16); };
    inline void FlipFirstCard() { m_vpclHand[0]->vFlip(); };
};

