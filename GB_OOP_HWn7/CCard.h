#pragma once
#include <iostream>

enum eCardValue
{
	eCV_Ace = 1,	// 1 or 11
	eCV_Two,		// 2
	eCV_Three,		// 3
	eCV_Four,		// 4
	eCV_Five,		// 5
	eCV_Six,		// 6
	eCV_Seven,		// 7
	eCV_Eight,		// 8
	eCV_Nine,		// 9
	eCV_Ten,		// 10
	eCV_Jack,		// 10
	eCV_Queen,		// 10
	eCV_King		// 10
};

enum eCardSuit
{
	eCS_Diamond = 1,
	eCS_Heart,
	eCS_Club,
	eCS_Spade
};

class CCard
{
public:
	CCard(eCardValue, eCardSuit, bool);
	CCard();
	~CCard();
	void vFlip() { m_blIsShowed = !m_blIsShowed; };
	int nGetValue();
	friend std::ostream& operator<< (std::ostream&, CCard&);
	inline bool blIsShowed() { return m_blIsShowed; };

private:
	eCardValue m_eCV_Value;
	eCardSuit m_eCS_Suit;
	bool m_blIsShowed;
};

