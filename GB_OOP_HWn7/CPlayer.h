#pragma once
#include "CGenericPlayer.h"

class CPlayer :
    public CGenericPlayer
{
public:
    CPlayer();
    CPlayer(std::string);
    ~CPlayer();

    virtual bool blIsHitting() const;
    void vWin() const { std::cout << "Congrats! You're winner!\n"; };
    void vLose() const { std::cout << "Unfortunately, you lose\n"; };
    void vPush() const { std::cout << "Tie ending\n"; };
};

