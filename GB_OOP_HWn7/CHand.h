#pragma once
#include <vector>
#include "CCard.h"

class CHand
{
public:
	CHand() {};
	virtual ~CHand() {};

	inline void vAdd(CCard* pclNew) { m_vpclHand.push_back(pclNew); };
	void vClear();
	int nGetValue();

protected:
	std::vector<CCard*> m_vpclHand;

private:
	bool m_blIsAce;
	int m_nHandValue;
};

