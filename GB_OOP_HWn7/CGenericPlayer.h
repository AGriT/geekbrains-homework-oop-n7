#pragma once
#include <string>
#include <iostream>
#include "CHand.h"

class CGenericPlayer :
    public CHand
{
public:
    CGenericPlayer();
    CGenericPlayer(std::string);
    virtual ~CGenericPlayer();

    virtual bool blIsHitting() = 0;
    inline bool blIsBoosted() { return (nGetValue() > 21); };
    inline void vBusted() { std::cout << m_strName << " are busted" << std::endl; };

    friend std::ostream& operator<< (std::ostream&, CGenericPlayer&);

private:
    std::string m_strName;
};

