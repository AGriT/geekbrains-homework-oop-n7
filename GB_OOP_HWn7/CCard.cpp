#include "CCard.h"

CCard::CCard(eCardValue eCVCurrent, eCardSuit eCSCurrent, bool blIsShowed)
	: m_eCV_Value(eCVCurrent),
	m_eCS_Suit(eCSCurrent),
	m_blIsShowed(blIsShowed)
{
}

CCard::CCard()
	: m_eCV_Value(eCardValue::eCV_Ace),
	m_eCS_Suit(eCardSuit::eCS_Diamond),
	m_blIsShowed(false)
{
}

CCard::~CCard()
{
}

int CCard::nGetValue()
{
	if ((int)m_eCV_Value <= 10)
		return (int)m_eCV_Value;
	else
		return 10;
}

std::ostream& operator<< (std::ostream& osOut, CCard& clCurrentCard)
{
	if (!(clCurrentCard.m_blIsShowed))
	{
		osOut << "XX";
		return osOut;
	}

	switch (clCurrentCard.m_eCV_Value)
	{
	case eCardValue::eCV_Ace:
		osOut << 'A';
		break;
	case eCardValue::eCV_Two:
		osOut << '2';
		break;
	case eCardValue::eCV_Three:
		osOut << '3';
		break;
	case eCardValue::eCV_Four:
		osOut << '4';
		break;
	case eCardValue::eCV_Five:
		osOut << '5';
		break;
	case eCardValue::eCV_Six:
		osOut << '6';
		break;
	case eCardValue::eCV_Seven:
		osOut << '7';
		break;
	case eCardValue::eCV_Eight:
		osOut << '8';
		break;
	case eCardValue::eCV_Nine:
		osOut << '9';
		break;
	case eCardValue::eCV_Ten:
		osOut << '10';
		break;
	case eCardValue::eCV_Jack:
		osOut << 'J';
		break;
	case eCardValue::eCV_Queen:
		osOut << 'Q';
		break;
	case eCardValue::eCV_King:
		osOut << 'K';
		break;
	default:
		osOut << 'E'; //Error-type
		break;
	}

	switch (clCurrentCard.m_eCS_Suit)
	{
	case eCardSuit::eCS_Heart:
		osOut << '\x003';
	case eCardSuit::eCS_Diamond:
		osOut << '\x004';
	case eCardSuit::eCS_Club:
		osOut << '\x005';
	case eCardSuit::eCS_Spade:
		osOut << '\x006';

	default:
		osOut << 'E'; //Error-type
		break;
	}

	return osOut;
}