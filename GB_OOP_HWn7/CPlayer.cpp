#include <conio.h>
#include "CPlayer.h"


CPlayer::CPlayer()
	: CGenericPlayer()
{
}

CPlayer::CPlayer(std::string strName)
	: CGenericPlayer(strName)
{
}

CPlayer::~CPlayer()
{
}

bool CPlayer::blIsHitting() const
{
	std::cout << "One more? (y/n)\n";
	char cAnswer;

	while (true)
	{
		cAnswer = _getch();

		if (cAnswer == 'y')
			return true;
		else if (cAnswer == 'n')
			return false;
		else
			std::cout << "Incorrect answer. Try Again\n";
	}

}